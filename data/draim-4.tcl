#
# * GEOSYNCHRONOUS CONSTELLATIONS / ELLIPTICAL SYSTEMS / HIGH EARTH ORBIT
# *
# * Draim example 4-satellite elliptical constellation
# *
# * Uses elliptical orbits for continuous coverage.
# *
# * If the Earth is completely enclosed within a tetrahedron,
# * then satellites at the vertices of that tetrahedron can
# * see the whole surface of the Earth.
# *
# * Project forwards 48 hours then record ground tracks
# * to see repeating paths from geosynchronous orbits.
# *
# * John E. Draim, A common-period four-satellite continuous
# * global coverage constellation, AIAA Journal of Guidance,
# * Control and Dynamics, vol. 10, no. 5, September-October 1987,
# * pp. 492-499. Orbital parameters from Table 1.
# *
# * John E. Draim, Tetrahedral multi-satellite continuous-coverage
# * constellation, US Patent 4,854,527, August 1989.
# http://www.freepatentsonline.com/4854527.html
# *
# * Draim later used this SaVi script to demonstrate his design:
#
# * John E. Draim et al., Common-period four-satellite continuous
# * global coverage constellations revisited, Advances in the
# * Astronautical Sciences, vol. 143, pp. 667-686, January 2012.
# *
# * A later paper in Nature made the Draim four-points idea more
# * practical by using orbits requiring less station-keeping. See:
# * Singh, L.A., Whittecar, W.R., DiPrinzio, M.D. et al.,
# * Low cost satellite constellations for nearly continuous
# * global coverage, Nature Communications, volume 11,
# * article 200, January 2020.
# https://dx.doi.org/10.1038/s41467-019-13865-0
# https://www.nature.com/articles/s41467-019-13865-0
#
# $Id: draim-4.tcl 175 2020-05-03 11:30:30Z lloydwood $

set NUM_PLANES 4

# apogee altitude 42203 nautical miles - translate to km
set apogee_altitude 78160.0

# perigee altitude 23193 nautical miles - translate to km
set perigee_altitude 42953.5

# setup orbital elements
set a [expr ($apogee_altitude+$perigee_altitude)/2+$RADIUS_OF_EARTH]

# e is approximately 0.263
set e [expr ($apogee_altitude-$perigee_altitude)/(2*$a)]

set inc 31.3
set omega -90

set coverage_angle 0.0

# compute period of orbit
set T_per [expr 2 * $PI * pow($a,1.5) / sqrt($MU)]

satellites GV_BEGIN
for {set j 0} {$j < $NUM_PLANES} {incr j} {
    set Omega [expr $j * 360.0 / $NUM_PLANES]
    set omega [expr -$omega]
    set T [expr $T_per / $NUM_PLANES * $j ]
    satellites LOAD $a $e $inc $Omega $omega $T "Draim-$j"
}
satellites GV_END
