#
# * GEOSYNCHRONOUS CONSTELLATIONS / ELLIPTICAL
# *
# * Singh four-satellite system with 48-hour orbits
# *
# * Project forwards 48 hours then record ground tracks
# * to see repeating paths from geosynchronous orbits.
# *
# * Singh, L.A., Whittecar, W.R., DiPrinzio, M.D. et al.,
# * Low cost satellite constellations for nearly continuous
# * global coverage, Nature Communications, volume 11,
# * article 200, January 2020.
# https://dx.doi.org/10.1038/s41467-019-13865-0
# https://www.nature.com/articles/s41467-019-13865-0
# * Orbital information is from the supplementaty information
# * to this paper.
# *
# $Id$

set NUM_PLANES 4


# altitude for all satellites
set a 66931.2

set e_list {0.030 0.018 0.018 0.018}
set inc_list {86.49 86.81 88.12 85.26}
set Omega_list {166.79 114.81 67.67 116.19}
set omega_list {134.31 153.05 148.86 155.39}
set v0_list {180.00 36.82 175.18 306.35}


set coverage_angle 0.0

# compute period of orbit
set T_per [expr 2 * $PI * pow($a,1.5) / sqrt($MU)]

# "The epoch for the elements as presented is November 26,
#  1995 at midnight GMT" - and SaVi starts at midnight GMT.
#  So, no adjustment in RAAN is needed?
set offset 0.0

satellites GV_BEGIN
for {set j 0} {$j < $NUM_PLANES} {incr j} {
    set Omega [expr [lindex $Omega_list $j] + $offset]

    set e [lindex $e_list $j]
    set inc [lindex $inc_list $j]
    set omega [lindex $omega_list $j]

    set anom [lindex $v0_list $j]

    # negative, as it's time TO periapsis (or, for Earth, perigee).
    set T [expr $T_per * -$anom / 360.0]

    set number [expr $j+1]
    satellites LOAD $a $e $inc $Omega $omega $T "Singh-48hr-$number"
}
satellites GV_END
