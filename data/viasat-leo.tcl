#
# * PROPOSED LARGE BROADBAND CONSTELLATIONS - 2010s MEGACONSTELLATIONS
# *
# * ViaSat LEO
# *
# * Originally described in ViaSat's filing to the US FCC: Petition
# * for Declaratory Ruling, SAT-LOI-20161115-00120, 15 November 2016,
# * as a Medium Earth Orbit system.
# *
# * Later reduced from 24 to 20 satellites (four planes of five
# * satellites) at the same altitude, in a November 2018 modification,
# * modelled in a separate script.
# *
# * The application was again later modified in May 2020 to be this
# * LEO system.
#
# this script for SaVi by Lloyd Wood (lloydwood@users.sourceforge.net)
# http://savi.sourceforge.net/
#
# $Id$

set SATS_PER_PLANE 36
set NUM_PLANES 8
set INTERPLANE_SPACING 45.0
set PHASING 0.0

# setup orbital elements
set a [expr 1300.0+$RADIUS_OF_EARTH]
set e 0.0
set inc 45.0
set omega 0.0
set T_per [expr 2 * $PI * pow($a,1.5) / sqrt($MU)]

# Coverage angle remains 25 degrees despite the MEO/LEO change.
set coverage_angle 25.0

satellites GV_BEGIN

# Plane offset is really a function of harmonic factor in Ballard constellations.
# (Rosette Constellations of Earth Satellites, A. H. Ballard, TRW,
# IEEE Transactions on Aerospace and Electronic Systems, Vol 16 No 5, Sep. 1980)
# 360 / 8 / 36 = 360 / 288 = 1.25 degrees approx.
# Application confirms 1st harmonic, with 1.25 degrees between planes.

set interplane_phasing [expr 360.0 / $NUM_PLANES / $SATS_PER_PLANE]

for {set j 0} {$j < $NUM_PLANES} {incr j} {
	set Omega [expr $j * $INTERPLANE_SPACING ]
	set plane_offset [expr ($T_per / 360) * ($j * $interplane_phasing)]

	for {set i 0} {$i < $SATS_PER_PLANE} {incr i} {
	   set T [expr ($T_per * $i / $SATS_PER_PLANE) - $plane_offset ]
	   set n [satellites LOAD $a $e $inc $Omega $omega $T "ViaSat LEO ($j, $i)"]
	   if {$i > 0} {satellites ORBIT_SET $n 0}
	}
}
satellites GV_END
