#
# * PROPOSED LARGE BROADBAND CONSTELLATIONS - 2010s MEGACONSTELLATIONS
# *
# * Telesat Lightspeed
# *
# * Canadian system that combines a Ballard rosette, good at coverage of
# * mid-latitudes, with a crude polar star to ensure coverage of the highest
# * latitudes.
# *
# * This is the crude polar star component.
# * 
# * This is based on Telesat's patent filing:
#
# * Dual leo satellite system and method for global coverage,
# * David Wending, Telesat Canada, WO2017177343A1, October 2017.
# https://patents.google.com/patent/WO2017177343A1/en
# *
# * See also https://www.telesat.com/services/leo/why-leo
# *
# * Uses intersatellite links, which are not yet simulated here.

# $Id: telesat-polar.tcl 181 2021-02-13 11:54:42Z lloydwood $

set SATS_PER_PLANE 12
set NUM_PLANES 6

# not optimised, and therefore not actually a Walker
# star, but that's what the patent application specifies.
# While any equatorial gaps or seam spacing can be covered
# by the rosette, crude star coverage here looks very weak.
set INTERPLANE_SPACING 30

# setup orbital elements
set a [expr 1000.0+$RADIUS_OF_EARTH]
set e 0.0
set inc 99.5
set omega 0.0

# assumes both constellations are active.
# does not look good.
set coverage_angle 20

# compute period of orbit
set T_per [expr 2 * $PI * pow($a,1.5) / sqrt($MU)]


satellites GV_BEGIN
for {set j 0} {$j < $NUM_PLANES} {incr j} {
	set Omega [expr $j * $INTERPLANE_SPACING]
	for {set i 0} {$i < $SATS_PER_PLANE} {incr i} {
		if { $j % 2 == 0} {
			set plane_offset 0
		} else {
			set plane_offset [expr $T_per / $SATS_PER_PLANE / 2.0]
		}
		set T [expr $T_per * $i / $SATS_PER_PLANE + $plane_offset]
		set n [satellites LOAD $a $e $inc $Omega $omega $T "Telesat-polar ($j, $i)"]
		if {$i > 0} {satellites ORBIT_SET $n 0}
	}
}
satellites GV_END

