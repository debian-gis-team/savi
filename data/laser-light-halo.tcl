#
# * NON-GEOSTATIONARY EQUATORIAL RING - PROPOSED SYSTEM
# *
# * LASER LIGHT COMMUNICATIONS aka aka HALO GLOBAL NETWORK
# *
# * See https://www.laserlightcomms.com/
# *
# * Medium Earth Orbit system using lasers for data delivery.
# *
# * First commercial space-based FSO laser communication network
# * to deploy in 2017, LaserFocusWorld, 12 September 2012.
# * gives the altitude.
# *
# * Uses intersatellite links, which are not yet simulated here.
# https://www.laserfocusworld.com/test-measurement/research/article/16565254/first-commercial-spacebased-fso-laser-communication-network-to-deploy-in-2017

# this script for SaVi by Lloyd Wood (lloydwood@users.sourceforge.net)
#
# $Id: laser-light-halo.tcl 105 2019-09-22 10:24:29Z lloydwood $

# 8 to 12 satellites
set NUM_SATS 12

# setup orbital elements
set a [expr 10500.0+$RADIUS_OF_EARTH]
set e 0.0
set inc 0.0
set omega 0.0
set T_per [expr 2 * $PI * pow($a,1.5) / sqrt($MU)]

# #without knowing limits on lasers, assume limit of visibility.
set coverage_angle 0.0

satellites GV_BEGIN

set T [expr $T_per ]
for {set j 0} {$j < $NUM_SATS} {incr j} {
	set Omega [expr $j * 360.0 / $NUM_SATS]
	satellites LOAD $a $e $inc $Omega $omega $T "Laser Light-$j"
}
satellites GV_END
