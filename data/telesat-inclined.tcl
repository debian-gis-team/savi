#
# * PROPOSED LARGE BROADBAND CONSTELLATIONS - 2010s MEGACONSTELLATIONS
# *
# * Telesat Lightspeed
# *
# * Canadian system that combines a Ballard rosette, good at coverage of
# * mid-latitudes, with a crude polar star to ensure coverage of highest
# * latitudes.
#  *
# * This is the inclined Ballard rosette component.
# *
# * This is based on Telesat's patent filing:
# 
# * Dual leo satellite system and method for global coverage,
# * David Wending, Telesat Canada, WO2017177343A1, October 2017.
# https://patents.google.com/patent/WO2017177343A1/en
# *
# * See also https://www.telesat.com/services/leo/why-leo
# *
# * Uses intersatellite links, which are not yet simulated here.
#
# $Id: telesat-inclined.tcl 181 2021-02-13 11:54:42Z lloydwood $

set SATS_PER_PLANE 6
set NUM_PLANES 9

# setup orbital elements
set a [expr 1250.0+$RADIUS_OF_EARTH]
set e 0.0
set inc 37.4
set omega 0.0
set T_per [expr 2 * $PI * pow($a,1.5) / sqrt($MU)]

set coverage_angle 10.0

# The patent filing doesn't metnion Walker, but at least it says Ballard.
# Plane offset is really a function of harmonic factor in Ballard constellations.
# (Rosette Constellations of Earth Satellites, A. H. Ballard, TRW,
# IEEE Transactions on Aerospace and Electronic Systems, Vol 16 No 5, Sep. 1980)
# 360 / 9 / 5 = 360 / 45 = 8 degrees approx.
#
satellites GV_BEGIN
for {set j 0} {$j < $NUM_PLANES} {incr j} {
	set Omega [expr $j * 360.0 / $NUM_PLANES]
	for {set i 0} {$i < $SATS_PER_PLANE} {incr i} {
		set T [expr $T_per * ( double($i) / $SATS_PER_PLANE - 8.0/360.0*$j) ]
		set n [satellites LOAD $a $e $inc $Omega $omega $T "Telesat-inclined ($j, $i)"]
		if {$i > 0} {satellites ORBIT_SET $n 0}
	}
}
satellites GV_END
