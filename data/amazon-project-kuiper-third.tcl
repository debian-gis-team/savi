#
# * PROPOSED LARGE BROADBAND CONSTELLATIONS - 2010s MEGACONSTELLATIONS
# *
# * Project Kuiper
# *
# * This is the third shell to be deployed in the Amazon Project Kuiper
# * system.
# *
# * This is based on the 4 July 2019 FCC filing.
# * This is at Ka-band - even though the name may suggest 'Ku'.
# *
# * Despite the number of satellites, the filing does not appear
# * to describe intersatellite links. The Technical Appendix says:
# * "The number of United States gateways sites will be approximately
# * equal to the number of active satellites serving U.S. territory."
#
# July FCC filings
# https://licensing.fcc.gov/cgi-bin/ws.exe/prod/ib/forms/attachment_menu.hts?id_app_num=131001&acct=322741&id_form_num=12&filing_key=-434810
#
# Amazon asks FCC for approval of Project Kuiper broadband satellite operation,
# Alan Boyle and Taylor Soper, GeekWire, July 6, 2019.
# https://www.geekwire.com/2019/amazon-asks-fcc-approval-project-kuiper-broadband-satellite-operation/
# https://finance.yahoo.com/news/amazon-asks-fcc-approval-project-175804004.html
#
# $Id: amazon-project-kuiper-third.tcl 84 2019-08-19 05:01:03Z lloydwood $

set SATS_PER_PLANE 28
set NUM_PLANES 28

# No initial deployment of half of shell
puts stderr "SaVi: Kuiper: full third shell"

# setup orbital elements
set a [expr 590.0 + $RADIUS_OF_EARTH]
set e 0.0
set inc 33.0
set omega 0.0
set T_per [expr 2 * $PI * pow($a,1.5) / sqrt($MU)]

# Minimum elevation angle is said to be 35 degrees.
set coverage_angle 35.0

upvar #0 NUM_COLORS NUM_COLORS

if {$NUM_COLORS < 19} {
    # more than 19 satellites can be visible in fisheye over mid-latitudes
    puts stderr "\nSaVi: Coverage view of Kuiper constellation benefits from largest number of colors (19+)."
}

# FIX THIS
# Plane offset is really a function of harmonic factor in Ballard constellations.
# (Rosette Constellations of Earth Satellites, A. H. Ballard, TRW,
# IEEE Transactions on Aerospace and Electronic Systems, Vol 16 No 5, Sep. 1980)
# 360 / 28 / 28 = 360 / 784 = 0.4592 degrees approx.
# The Access .ndb database file in the July FCC filing would suggest
# exactly which harmonic to use. Technical document suggests simple tiling
# with half spacing given number of sats, i.e. 14th harmonic.

set interplane_phasing [expr 360.0 / $NUM_PLANES / $SATS_PER_PLANE * 17]

satellites GV_BEGIN
for {set j 0} {$j < $NUM_PLANES} {incr j} {
	set Omega [expr $j * 360.0 / $NUM_PLANES]

	for {set i 0} {$i < $SATS_PER_PLANE} {incr i} {
		set plane_offset [expr ($T_per / 360) * ($j * $interplane_phasing) ]

		set T [expr ($T_per * $i / $SATS_PER_PLANE ) + $plane_offset ]
		set n [satellites LOAD $a $e $inc $Omega $omega $T "Kuiper-3rd ($j, $i)"]
		if {$i > 0} {satellites ORBIT_SET $n 0}
	}
}
satellites GV_END
