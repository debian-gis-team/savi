#
# * PROPOSED LARGE BROADBAND CONSTELLATIONS - 2010s MEGACONSTELLATIONS
# *
# * LeoSat
# *
# * A Walker polar star geometry, using optical intersatellite
# * links with the Iridium NEXT bus. Note the 'orbital seam' where
# * the footprints of ascending (going north over the Equator) and
# * descending (going (south) satellites overlap more to ensure
# * continuous coverage.
# *
# * See Petition for Declatory Ruling, LeoSat MA, Inc., US FCC
# * SAT-LOI-20161115-00112, 15 November 2016.
# *
# * "It's 78-108 satellites in polar orbit at 1,430 kilometers in
# * altitude, six orbital planes with 18 satellites each."
# * -- Mark Rigolle, CEO of LeoSat, interviewed by Peter B. de Selding
# * in "Never Mind the Unconnected Masses, LeoSat’s Broadband Constellation
# * is Strictly Business," Space News, 20 November 2015.
# http://spacenews.com/nevermind-the-unconnected-masses-leosats-broadband-constellation-is-strictly-business/
# *
# * FCC letter SAT-PDR-20161115-00112 of 15 November 2018 clains
# * 78 satellites at 90 deg inclination in 6 1400km orbital planes,
# * plus 6 in-orbit spares - i.e. the minimum of the range.
# *
# * LeoSat ceased development in August 2019 (LeoSat, absent
# * investors, shuts down, Caleb Henry, SpaceNews, 13 November 2019).
# https://spacenews.com/leosat-absent-investors-shuts-down/
# *
# * See http://leosat.com/
# *
# * Planned intersatellite links, which are not yet simulated here.

# $Id: leosat.tcl 130 2019-11-25 04:08:24Z lloydwood $

set SATS_PER_PLANE 13
set NUM_PLANES 6

# this is a rough approximation
set INTERPLANE_SPACING 31.0

# setup orbital elements
# could be retrograde sun synchronous orbit to simplify solar panel
# tracking, at circa 110 degrees, but seems unlikely.
set a [expr 1430.0+$RADIUS_OF_EARTH]
set e 0.0

# previously 87 deg, but FCC letter says 90.
set inc 90.0

set omega 0.0

# needs relatively low elevation angle for terminals.
# Technical Annex says 10 degrees, though 25 suposedly gives full coverage.
set coverage_angle 10.0

# compute period of orbit
set T_per [expr 2 * $PI * pow($a,1.5) / sqrt($MU)]


satellites GV_BEGIN
for {set j 0} {$j < $NUM_PLANES} {incr j} {
	set Omega [expr $j * $INTERPLANE_SPACING]
	for {set i 0} {$i < $SATS_PER_PLANE} {incr i} {
		if { $j % 2 == 0} {
			set plane_offset 0
		} else {
			set plane_offset [expr $T_per / $SATS_PER_PLANE / 2.0]
		}
		set T [expr $T_per * $i / $SATS_PER_PLANE + $plane_offset]
		set n [satellites LOAD $a $e $inc $Omega $omega $T "LeoSat ($j, $i)"]
		if {$i > 0} {satellites ORBIT_SET $n 0}
	}
}
satellites GV_END

