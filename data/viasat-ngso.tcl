#
# * MEDIUM EARTH ORBIT (MEO) SYSTEMS - PROPOSAL
# *
# * ViaSat NGSO
# *
# * Originally described in ViaSat's filing to the US FCC: Petition
# * for Declaratory Ruling, SAT-LOI-20161115-00120, 15 November 2016.
# * Later reduced from 24 to 20 satellites (four planes of five
# * satellites) at the same altitude, in a November 2018 modification.
# *
# * The application was again later modified in May 2020 to be a LEO
# * system, modelled in a separate script.
#
# this script for SaVi by Lloyd Wood (lloydwood@users.sourceforge.net)
# http://savi.sourceforge.net/
#
# https://spacenews.com/viasat-shrinks-meo-constellation-plans/
# Viasat shrinks MEO constellation plans, Caleb Henry, 5 November 2018.
#
# $Id: viasat-ngso.tcl 177 2020-05-27 04:40:43Z lloydwood $

set SATS_PER_PLANE 8
set NUM_PLANES 3
set INTERPLANE_SPACING 120
set PHASING 0.0

# setup orbital elements
set a [expr 8200.0+$RADIUS_OF_EARTH]
set e 0.0
set inc 87.0
set omega 0.0
set T_per [expr 2 * $PI * pow($a,1.5) / sqrt($MU)]

# in Appendix A.
set coverage_angle 25.0

satellites GV_BEGIN

for {set j 0} {$j < $NUM_PLANES} {incr j} {
	set Omega [expr $j * $INTERPLANE_SPACING ]
	if { $j == 1 } {
	   set PHASING 60
        }
	if { $j == 2 } {
	   set PHASING 30
        }
	set T_PHASE [expr $T_per * $PHASING / 360.0 ]
	for {set i 0} {$i < $SATS_PER_PLANE} {incr i} {
	   set T [expr $T_per * $i / $SATS_PER_PLANE + $T_PHASE ]
	   set n [satellites LOAD $a $e $inc $Omega $omega $T "ViaSat NGSO ($j, $i)"]
	   if {$i > 0} {satellites ORBIT_SET $n 0}
	}
}
satellites GV_END
