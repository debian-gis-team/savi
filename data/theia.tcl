#
# * PROPOSED REMOTE SENSING SYSTEMS
# *
# * Theia
# *
# * Theia Holdings plans a large remote sensing constellation,
# * which follows a crude polar star geometry.
# *
# * This follows the spcecification set out in the technical
# * narrative related to SAT-LOA-20161115-00121, April 2016.
# *
# * FCC Approves Theia’s 112-Satellite Earth Imaging Constellation,
# * Doug Messier, Parabolic Arc, 10 May 2019.
# http://www.parabolicarc.com/2019/05/10/fcc-approves-theias-112satellite-earth-imaging-constellation/
# *
# * Uses intersatellite links, which are not yet simulated here.

# $Id: theia.tcl 97 2019-09-19 21:02:36Z lloydwood $

set SATS_PER_PLANE 14
set NUM_PLANES 8

# this is what their narrative says. 14 planes = 343 degrees,
# which is less than 360. No Walker seam overlap here, but then
# it's retrograde.
set INTERPLANE_SPACING 24.5

# setup orbital elements
set a [expr 800.0+$RADIUS_OF_EARTH]
set e 0.0
set inc 98.6
set omega 0.0

# this appears to be an effective minimum for communications.
# Remote-sensing swath widths would be far narrower.
set coverage_angle 15

# compute period of orbit
set T_per [expr 2 * $PI * pow($a,1.5) / sqrt($MU)]


satellites GV_BEGIN
for {set j 0} {$j < $NUM_PLANES} {incr j} {
	set Omega [expr $j * $INTERPLANE_SPACING]
	for {set i 0} {$i < $SATS_PER_PLANE} {incr i} {
		if { $j % 2 == 0} {
			set plane_offset 0
		} else {
			set plane_offset [expr $T_per / $SATS_PER_PLANE / 2.0]
		}
		set T [expr $T_per * $i / $SATS_PER_PLANE + $plane_offset]
		set n [satellites LOAD $a $e $inc $Omega $omega $T "Theia ($j, $i)"]
		if {$i > 0} {satellites ORBIT_SET $n 0}
	}
}
satellites GV_END

