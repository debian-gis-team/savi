#
# * PROPOSED LARGE BROADBAND CONSTELLATIONS - 2010s MEGACONSTELLATIONS
# *
# * Telesat Lightspeed
# *
# * Canadian system that combines a Ballard rosette, good at coverage of
# * mid-latitudes, with a crude polar star to ensure coverage of the highest
# * latitudes.
# *
# * This script loads in both separate constellations. As those
# * are at different altitudes and inclinations, they're assumed
# * to be unsynchronised.
# *
# * This is based on Telesat's patent filing:
# 
# * Dual leo satellite system and method for global coverage,
# * David Wending, Telesat Canada, WO2017177343A1, October 2017.
# https://patents.google.com/patent/WO2017177343A1/en
# *
# * See also https://www.telesat.com/services/leo/why-leo
# *
# * Named as Lightspeed in February 2021, when Thales Alenia Space
# * was announced as the selected builder.
# *
# * Uses intersatellite links, which are not yet simulated here.
# *
# $Id: telesat-all.tcl 182 2021-02-13 12:38:59Z lloydwood $

satellites GV_BEGIN

source "data/telesat-polar.tcl"
source "data/telesat-inclined.tcl"

satellites GV_END
