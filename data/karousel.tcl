#
# * GEOSYNCHRONOUS CONSTELLATIONS / ELLIPTICAL / MEDIUM EARTH ORBIT (MEO) SYSTEMS - PROPOSAL
# *
# * Karousel elliptical broadcast constellation
# *
# * Project forwards 6 hours then record ground tracks
# * to see repeating paths from geosynchronous orbits.
# *
# * From information in their April 2016 FCC Application,
# * which was authorised 16 August 2018.
# * See document SAT-LOA-20161115-00113
# *
# $Id: karousel.tcl 175 2020-05-03 11:30:30Z lloydwood $

set NUM_PLANES 12

# figures given are actually altitudes.
set apogee [expr 40002.3+$RADIUS_OF_EARTH]
set perigee [expr 31569.5+$RADIUS_OF_EARTH]

# setup orbital elements
set a [expr ($apogee + $perigee)/2]

set e [expr ($apogee - $perigee)/(2*$a)]

# for all satellites
set inc 63.4
set omega 180.0

# taken directly from SATLOA2016111500113
set anomaly {0.0 90.0 180.0 0.0 90.0 180.0 0.0 90.0 180.0 270.0 270.0 270.0}

# ordered differently so that satellites fall in correct loops over continents,
# as SaVi initial Earth rotation is different from the rotation offset at the FCC TLE.
set RAAN {149.894 59.894 329.894 39.894 309.894 219.894 259.894 169.894 79.894 239.894 129.894 349.894}

# FCC application, p. 70: "elevation angles to Karousel satellites will be high
# relative to other systems." So, old Ka-band favourite of 40 degrees mask.
set coverage_angle 40.0

# compute period of orbit
set T_per [expr 2 * $PI * pow($a,1.5) / sqrt($MU)]

# adjust for different rotation of Earth in FCC filing vs SaVi
# just +55 sets up loops in the right place, but we need to reorder RAAN to match
# the plot in the figure on p. 29 of the 15 November 2016 Karousel FCC filing.
set offset 55.0

satellites GV_BEGIN
for {set j 0} {$j < $NUM_PLANES} {incr j} {
    # adjust for different rotation of Earth in FCC filing vs SaVi
    # just +55 sets up loops in the right place, but we need to reorder RAAN to match
    # the plot in the figure on p. 29 of the 15 November 2016 Karousel FCC filing.
    set Omega [expr [lindex $RAAN $j] + $offset]

    set anom [lindex $anomaly $j]

    # negative, as it's time TO periapsis (or, for Earth, perigee).
    set T [expr $T_per * -$anom / 360.0]

    set number [expr $j+1]
    satellites LOAD $a $e $inc $Omega $omega $T "Karousel-$number"
}
satellites GV_END
