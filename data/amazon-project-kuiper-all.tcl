#
# * PROPOSED LARGE BROADBAND CONSTELLATIONS - 2010s MEGACONSTELLATIONS
# *
# * Project Kuiper
# *
# * This is the full Amazon Project Kuiper system, consisting
# * of three separate Ballard rosette 'shells'. Each shell
# * is loaded separately from a different script.
# *
# * This is based on the 4 July 2019 FCC filing.
# * This is at Ka-band - even though the name may suggest 'Ku'.
# *
# * Despite the number of satellites, the filing does not appear
# * to describe intersatellite links. The Technical Appendix says:
# * "The number of United States gateways sites will be approximately
# * equal to the number of active satellites serving U.S. territory."
#
# July FCC filings
# https://licensing.fcc.gov/cgi-bin/ws.exe/prod/ib/forms/attachment_menu.hts?id_app_num=131001&acct=322741&id_form_num=12&filing_key=-434810
#
# Amazon asks FCC for approval of Project Kuiper broadband satellite operation,
# Alan Boyle and Taylor Soper, GeekWire, July 6, 2019.
# https://www.geekwire.com/2019/amazon-asks-fcc-approval-project-kuiper-broadband-satellite-operation/
# https://finance.yahoo.com/news/amazon-asks-fcc-approval-project-175804004.html
#
# $Id: amazon-project-kuiper-all.tcl 77 2019-08-16 06:25:18Z lloydwood $

satellites GV_BEGIN

source "data/amazon-project-kuiper-first.tcl"
source "data/amazon-project-kuiper-second.tcl"
source "data/amazon-project-kuiper-third.tcl"

satellites GV_END
