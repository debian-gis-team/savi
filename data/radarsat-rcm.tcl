#
# * REMOTE SENSING - OPERATIONAL SYSTEM
# *
# * RADARSAT-3 / RADARSAT CONSTELLATION MISSION
# *
# * Three satellites are spaced equally in a sun-synchronous plane,
# * which ascends over the Equator at 6:00 pm, in order after one
# * another. Launched June 2019. The earlier RADARSAT-1 and -2 are
# * not included here.
# *
# * This script turns on sunlight to show the sun-synchronous orbits.
# *
# * Satellite, payload, and orbital details from ESA's Earth
# * Observation Portal.
# https://earth.esa.int/web/eoportal/satellite-missions/r/rcm
#
# $Id: radarsat-rcm.tcl 111 2019-09-26 07:05:26Z lloydwood $

# sun-synchronous, so turn on sunlight
upvar #0 sun_flag sun_flag
set sun_flag 1

set a [expr 600.0+$RADIUS_OF_EARTH]

set inc 97.7
set e 0.0
set omega 0.0

# ascending over Equator at 18:00
set Omega [expr 18.00/12 * 180.0]

# 0 or 5 degrees would be minimum for ground station visibility.
# 600km swath width / accessible area.
# arctan(686/300) = 66.4 degrees - ignores curvature of Earth.
# or 23.6 for half-cones from satellites.
# We could set half-cone for correct fisheye behaviour, but then
# the popup and subsequently-loaded scripts are incorrect.

set coverage_angle 66.4

# set coverage_angle 23.6
# upvar #0 coverage_angle_flag coverage_angle_flag
# set coverage_angle_flag 1
# puts stderr "\n\nSaVi: half-cone coverage angle of $coverage_angle deg shows extent of DMC imaging swath."

puts stderr "\nSaVi: imaging swath shown. Set mask angle to zero for limits of communications connectivity."

set SATS_PER_PLANE 3

# compute period of orbit
set T_per [expr 2 * $PI * pow($a,1.5) / sqrt($MU)]

satellites GV_BEGIN

for {set i 0} {$i < $SATS_PER_PLANE} {incr i} {
    set T [expr $T_per / $SATS_PER_PLANE * $i ]
    satellites LOAD $a $e $inc $Omega $omega $T "RADARSAT RMC-$i"
}

satellites GV_END
