#
# * GEOSYNCHRONOUS CONSTELLATIONS / ELLIPTICAL
# *
# * Singh four-satellite system with 24-hour orbits
# *
# * Project forwards 24 hours then record ground tracks
# * to see repeating paths from geosynchronous orbits.
# *
# * Singh, L.A., Whittecar, W.R., DiPrinzio, M.D. et al.,
# * Low cost satellite constellations for nearly continuous
# * global coverage, Nature Communications, volume 11,
# * article 200, January 2020.
# https://www.nature.com/articles/s41467-019-13865-0
# * Orbital information is from the supplementaty information
# * to this paper.
# *
# $Id$

set NUM_PLANES 4


# altitude for all satellites
set a 42163.6

set e_list {0.020 0.010 0.021 0.018}
set inc_list {75.71 73.88 77.86 78.16}
set Omega_list {71.03 269.68 13.57 335.59}
set omega_list {65.75 359.72 169.28 259.15}
set v0_list {180.00 325.35 231.50 271.44}


set coverage_angle 0.0

# compute period of orbit
set T_per [expr 2 * $PI * pow($a,1.5) / sqrt($MU)]

#  "The epoch for the elements as presented is November 26,
#   1995 at midnight GMT" - and SaVi starts at midnight GMT.
#   So, no adjustment in RAAN is needed?
set offset 0.0

satellites GV_BEGIN
for {set j 0} {$j < $NUM_PLANES} {incr j} {
    set Omega [expr [lindex $Omega_list $j] + $offset]

    set e [lindex $e_list $j]
    set inc [lindex $inc_list $j]
    set omega [lindex $omega_list $j]

    set anom [lindex $v0_list $j]

    # negative, as it's time TO periapsis (or, for Earth, perigee).
    set T [expr $T_per * -$anom / 360.0]

    set number [expr $j+1]
    satellites LOAD $a $e $inc $Omega $omega $T "Singh-24hr-$number"
}
satellites GV_END
