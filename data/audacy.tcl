#
# * MEDIUM EARTH ORBIT - GEOSYNCHRONOUS PROPOSED SYSTEM
# *
# * Audacy
# *
# * Intended for use as a relay system for data from
# * other satellites.
# *
# * How this can provide global coverage without the minimum
# * of four satellites as determined by Draim is a good question,
# * and 'the satellites we're receiving from area much higher'
# * is not the best answer to that question.
# *
# * Uncertain if this simulation attempt is correct, but the
# * minimal coverage overlap suggests it's along the right lines.
# *
# * Project forwards 24 hours then record ground tracks
# * to see repeating patha from geosynchronous orbits.
# *
# * FCC application document SAT-LOA-2016-1115-00117, April 2016.
# *
# * Uses intersatellite links, which are not yet simulated here.
# *
# $Id: audacy.tcl 160 2020-02-06 10:51:45Z lloydwood $

set NUM_PLANES 3

# figures given in the schedule are actually radii, explains the
# narrative. Unlike Karousel. You'd think the orbits would be higher,
# since with only three satellites there will be coverage
# gaps on Earth. For satellites at higher altitudes, less so.
set a 20270.4

# setup orbital elements

# for all satellites
set e 0.0
set inc 25.0
set omega 180.0

set anom 0

# SaVi initial Earth rotation is different from the rotation offset at the FCC TLE,
# but that shouldn't matter for these circular orbits.
# SAT-LOA-2016-1115-00117 gave one with a mean anomaly of 180 deg,
# so we just subtract 180 deg from it to get spacing of 120 degrees.
set RAAN {37.64 157.64 277.64}

# Uses spotbeams, so we set to limit of visibility.
# That matters more for receiving gateways,
# not for satellites originating data. Assume
# gateways are near-equatorial...
set coverage_angle 0.0

# compute period of orbit
set T_per [expr 2 * $PI * pow($a,1.5) / sqrt($MU)]

satellites GV_BEGIN
for {set j 0} {$j < $NUM_PLANES} {incr j} {
    # could adjust for different rotation of Earth in FCC filing vs SaVi
    set Omega [lindex $RAAN $j]

    # negative, as it's time TO periapsis (or, for Earth, perigee).
    set T [expr $T_per * (-$j * 120.0)/360.0]
 
    set number [expr $j+1]
    satellites LOAD $a $e $inc $Omega $omega $T "Audacy-$number"
}
satellites GV_END
