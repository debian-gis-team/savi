#
######################################################
#
#  SaVi by Lloyd Wood (lloydwood@users.sourceforge.net),
#          Patrick Worfolk (worfolk@alum.mit.edu) and
#          Robert Thurman.
#
#  Copyright (c) 1997 by The Geometry Center.
#  Also Copyright (c) 2017 by Lloyd Wood.
#
#  This file is part of SaVi.  SaVi is free software;
#  you can redistribute it and/or modify it only under
#  the terms given in the file COPYRIGHT which you should
#  have received along with this file.  SaVi may be
#  obtained from:
#  http://savi.sourceforge.net/
#  http://www.geom.uiuc.edu/locate/SaVi
#
######################################################
#
# coverage_size.tcl
#
# $Id: coverage_size.tcl 67 2019-08-12 10:03:05Z lloydwood $

proc coverage_size(build) {} {
    global IM_H
 
    if {[eval window(raise) coverage_size]} return

    set name [build_Toplevel coverage_size]

    wm protocol $name WM_DELETE_WINDOW coverage_size(dismiss)

    build_Title $name "Choose coverage size..."

    set cmd [build_CmdFrame $name cmd]

    set cmda [build_StdFrame $name cmd]

    set size $IM_H
    
    build_LabelEntryColumns $cmda colors \
      {ientry "Vertical height, up to 4096 pixels.\nLarger is slower, but provides more detail in Geomview.\nContinents are not drawn in the Coverage panel." {size}}

    bind $cmda.colors.c0.0 <Return> {coverage_size(set) $size}
    bind $cmda.colors.c0.0 <Tab> {coverage_size(set) $size}

    pack $cmda -expand 1 -fill x
    
    build_Buttonbar $name dbbar \
      {"Set coverage map size" "coverage_size(set) $size"} \
      {"Cancel" "coverage_size(dismiss)"}

    update
}

proc coverage_size(set) {size} {
    global IM_H IM_W

    if {$size == $IM_H} return
    if {($size < 0) || ($size > 5000)} return

    coverage(map_hide)

    set IM_H $size
    set IM_W [expr $IM_H * 2]
    
    satellites COVERAGE_MAP_OFF
    satellites COVERAGE_MAP_ON

    coverage_size(dismiss)
    coverage(map_show)
    coverage(update)
}

proc coverage_size(dismiss) {} {

    destroy .coverage_size
}

proc coverage_size(cancel) {} {

    coverage_size(dismiss)
}
