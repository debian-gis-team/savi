#
######################################################
#
#  SaVi by Lloyd Wood (lloydwood@users.sourceforge.net),
#          Patrick Worfolk (worfolk@alum.mit.edu) and
#          Robert Thurman.
#
#  Copyright (c) 1997 by The Geometry Center.
#  Also Copyright (c) 2017 by Lloyd Wood.
#
#  This file is part of SaVi.  SaVi is free software;
#  you can redistribute it and/or modify it only under
#  the terms given in the file COPYRIGHT which you should
#  have received along with this file.  SaVi may be
#  obtained from:
#  http://savi.sourceforge.net/
#  http://www.geom.uiuc.edu/locate/SaVi
#
######################################################
#
# params.tcl
#
# $Id: params.tcl 198 2023-04-16 08:54:04Z lloydwood $


proc params(build) {} {
    global PI params tparams rotation_period

    if {[eval window(raise) params]} return

    set name [build_Toplevel params]

    wm protocol $name WM_DELETE_WINDOW params(dismiss)

    build_Title $name "Simulation constants"

    set cmd [build_StdFrame $name cmd]

    set cmda [build_StdFrame $cmd a]

    # params(orbital_period) is around the Sun.
    # We show length of day as it is easier to understand.
    set rotation_period [expr 2.0*$PI/$params(Omega)]

    build_LabelEntryColumns $cmda le0 \
	    {text " " {"Radius (km)" "Rotational period (s)" "Tilt (degrees)"} } \
	    {dentry "Central body" {tparams(Radius) rotation_period tparams(tilt)} }

    pack $cmda -side left -expand 1

    set cmdb [build_StdFrame $cmd b]

    build_LabelEntryColumns $cmdb le1 \
	{text " " {"Mu (km^3/s^2)" "Oblateness (J2)"} } \
	{dentry "Gravitational model" {tparams(Mu) tparams(J2) } }

    pack $cmdb -side right -expand 1

    pack $cmd


    build_DismissButtonbar $name dbbar "params(dismiss)" \
	{"Reset to default" params(reset)} \
	{"Cancel" params(cancel)} \
	{"Apply" params(apply)}


    update

    # fill in the boxes
    params(cancel)
}

proc params(dismiss) {} {
    destroy .params
    params(cancel)
}

proc params(apply) {} {
    global PI params tparams rotation_period
    
    set inds [array names tparams]
    foreach ind $inds {
        set params($ind) $tparams($ind)
    }

    # Omega is definitive, we compute from display
    set params(Omega) [expr 2.0*$PI/$rotation_period]

    # now update other panels if necessary
    edit(update)
}

proc params(cancel) {} {
    global PI params tparams rotation_period

    # when inheriting all params, without messing for the user interface,
    # this mapping code is useful.
    
    set inds [array names tparams]
    foreach ind $inds {
        set tparams($ind) $params($ind)
    }

    # Omega is definitive, we return to display
    set rotation_period [expr 2.0*$PI/$params(Omega) ]
}

proc params(reset) {} {
    satellites RESET_PARAMS
    params(cancel)
}
