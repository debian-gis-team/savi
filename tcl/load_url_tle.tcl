#
######################################################
#
#  SaVi by Lloyd Wood (lloydwood@users.sourceforge.net),
#          Patrick Worfolk (worfolk@alum.mit.edu) and
#          Robert Thurman.
#
#  Copyright (c) 1997 by The Geometry Center.
#  Also Copyright (c) 2017 by Lloyd Wood.
#
#  This file is part of SaVi.  SaVi is free software;
#  you can redistribute it and/or modify it only under
#  the terms given in the file COPYRIGHT which you should
#  have received along with this file.  SaVi may be
#  obtained from:
#  http://savi.sourceforge.net/
#  http://www.geom.uiuc.edu/locate/SaVi
#
######################################################
#
# load_url_tle.tcl
#
# $Id: load_url_tle.tcl 203 2023-04-28 00:46:24Z lloydwood $

proc load_url_tle(init) {} {
    global url_choice load_url

    set url_choice 0
    set load_url "http://"
}

proc load_url_tle(build) {} {
    global url_choice load_url

    if {[eval window(raise) load_url_tle]} return

    set name [build_Toplevel load_url_tle]

    wm protocol $name WM_DELETE_WINDOW load_url_tle(dismiss)

    build_Title $name "Download constellation TLE set from URL..."

	set cmd [build_CmdFrame $name cmd]

	set cmda [build_StdFrame $name cmd]

	 build_Label $cmda text "Sourced from Celestrak unless stated."

    build_IPopupMenu $cmda p0 "" \
	url_choice load_url_tle(choice) { \
	    "Enter link to TLE:" \
	    "" \
	    "Iridium NEXT" \
            "Iridium" \
            "Globalstar" \
            "Orbcomm" \
	    "O3b Networks" \
	    "OneWeb" \
	    "SpaceX Starlink" \
            "" \
            "GPS" \
            "Glonass" \
      	    "Galileo" \
	    "Beidou" \
	    "" \
	    "Raduga" \
	    "Gorizont" \
       	    "Molniya" \
	    "TDRSS" \
	    "Cubesats" \
	    "" \
	    "Spire" \
	    "Swarm" \
	    "Planet Labs" \
	    "Planet Labs (from Planet Labs)"
        }
	pack $cmda.p0 -side left

    build_LabelEntryColumns $cmda url_path \
        {lentry "" {load_url} }

    bind $cmda.url_path.c0.0 <Return> {load_url_tle(load) $load_url}

    pack $cmda -expand 1 -fill x

    build_Buttonbar $name dbbar \
	{"Download elsets from web" {load_url_tle(load) $load_url} } \
	{"Close" "load_url_tle(dismiss)"}

    update
}

proc load_url_tle(choice) {} {
    global url_choice load_url

    # sources on web for TLEs:
    # http://celestrak.com/NORAD/elements/
    # http://www.idb.com.au/joomla/index.php
    #
    # Celestrak's own 3D TLE web viewer (globe icons on elements webpage)
    # is more impressive than SaVi.

    # we cannot yet fully support https: in Tcl.
    set load_url "http://"
    
    switch $url_choice {
        0 {
            # already set
        }
	1 {
	    # divider
	}
        2 {
            set load_url "http://celestrak.org/NORAD/elements/iridium-NEXT.txt"
        }
        3 {
	    set load_url "http://celestrak.org/NORAD/elements/iridium.txt"
	}
	4 {
            set load_url "http://celestrak.org/NORAD/elements/globalstar.txt"
        }
	5 {
            set load_url "http://celestrak.org/NORAD/elements/orbcomm.txt"
        }
	6 {
            set load_url "http://celestrak.org/NORAD/elements/other-comm.txt"
        }
	7 {
	    set load_url "http://celestrak.org/NORAD/elements/oneweb.txt"
        }
	8 {
	    set load_url "http://celestrak.org/NORAD/elements/starlink.txt"
	}
	9 {
	    # divider
	}
        10 {
            set load_url "http://celestrak.org/NORAD/elements/gps-ops.txt"
        }
        11 {
            set load_url "http://celestrak.org/NORAD/elements/glo-ops.txt"
        }
        12 {
            set load_url "http://celestrak.org/NORAD/elements/galileo.txt"
        }
	13 {
	    set load_url "http://celestrak.org/NORAD/elements/beidou.txt"
	}
	14 {
	    # divider
	}
	15 {
	    set load_url "http://celestrak.org/NORAD/elements/raduga.txt"
	}
	16 {
	    set load_url "http://celestrak.org/NORAD/elements/gorizont.txt"
	}
	17 {
	    set load_url "http://celestrak.org/NORAD/elements/molniya.txt"
	}
        18 {
            set load_url "http://celestrak.org/NORAD/elements/tdrss.txt"
        }
        19 {
            set load_url "http://celestrak.org/NORAD/elements/cubesat.txt"
        }
	20 {
	    # divider
	}
        21 {
	    set load_url "http://celestrak.org/NORAD/elements/spire.txt"
	}
	22 {
	    set load_url "http://celestrak.org/NORAD/elements/swarm.txt"
	}
	23 {
	    set load_url "http://celestrak.org/NORAD/elements/planet.txt"
	}
	24 {
	    set load_url "https://ephemerides.planet-labs.com/planet_mc.tle"
	}
    }
}

proc load_url_tle(lynx) {url localfile} {
    set lynx "lynx -dump"

    if {[catch {eval exec $lynx $url > $localfile}]} {
        puts stderr "\nSaVi: could not run $lynx successfully. Is $lynx installed?"
    } else {
        puts stderr "\nSaVi: lynx download of $url"
    }
}    

proc load_url_tle(load) {url} {

# https support requires third-party tls package by Matt Newman
# see http://wiki.tcl.tk/1475 and http://wiki.tcl.tk/2630

    set filename [file tail $url]

    # we append .tle because SaVi insists that tle files end .tle
    set localfile "/tmp/$filename.tle"
    set n 1
    while {[file exists $localfile]} {
        set localfile "/tmp/$filename-$n.tle"
        incr n
    }

    if {[catch {package require http}]} {
        puts stderr "\nSaVi: Tcl http package not found."
        set no_http_package 1
    } else {
        set no_http_package 0
    }

    set a [string first http: $url]
    if {$no_http_package || ($a < 0)} {
        load_url_tle(lynx) $url $localfile
    } else {
        set token [http::geturl $url -binary 1]

        if {[string compare [::http::code $token] "HTTP/1.1 200 OK"]} {
            puts stderr "\nSaVi: Tcl did not get $url"
	    load_url_tle(lynx) $url $localfile
        } else {
            set fo [open $localfile w]
            fconfigure $fo -translation binary
            puts -nonewline $fo [http::data $token]
            close $fo
	}

        ::http::cleanup $token
    }
    puts stderr "\nSaVi: downloaded \"$url\" to \"$localfile\""

    puts stderr "\nSaVi: replacing any existing satellites so that time 0 is consistent."
    main(delete_all)

    main(load_file) $localfile
}

proc load_url_tle(dismiss) {} {

    destroy .load_url_tle
}

proc load_url_tle(cancel) {} {

    load_url_tle(dismiss)
}
